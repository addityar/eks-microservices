# eks-microservice
to start using this script you need :
- AWS Account 
- Terraform installed on your local
- aws-cli installed on your local

for this case : 

# a. Case: We need to deploy a “Food Ordering” System based on this requirements:
i. Programming language<br>
1. Frontend: React.js
2. Backend: Node.js
3. Mobile app: React Native<br>

ii. Database: PostgreSQL


# b. Server requirements
i. Microservices using kubernetes (total: 5 services) and service mesh<br>
ii. Provide secure access into resources and service either from internal or external traffic<br>
iii. Analytic which capturing, analyzing, and securely storing transaction log data<br>
iv. CI/CD which optimize & support rapid development cycle

step to solve :

# deploy database
we need to create RDS for our database, and using PostgreSQL
- `cd terraform/RDS`
- `terraform init`
- `terraform apply`

# deploy EKS Cluster
we need to create EKS cluster to handle 5 services, example we have 4 backend services, and 1 frontend service. So, i create 1 EKS cluster with 2 desired nodes for running nodes in the beginning, and set max 3 nodes for max capacity, and make sure we secure EKS access only with spesific IP address, we can set allowed ip adress using security group and add SG to eks cluster : exaple our vpn IP is 192.168.1.1
- `cd terraform/EKS-cluster`
- `terraform init`
- `terraform apply` 

# Deploy Services 
In this section, we assume that your service has been build as docker image, and your service can connect to database.<br>

**1. Deploy all backend services**
- `cd EKS/backend`
- `kubectl apply -f backend1.yaml`
- `kubectl apply -f backend2.yaml`
- `kubectl apply -f backend3.yaml`
- `kubectl apply -f backend4.yaml`<br>

**2. Deploy frontend service**
- `cd EKS/frontend`
- `kubectl apply -f frontend1.yaml`
- if frontend service can be access to public you can apply ingress `cd ingress` `kubectl apply -f ingress-frontend1.yaml`<br>

**3. For Mobile service i have 2 option** 
- option 1 : developer can build react app on the local to generate .apk/.app/IOS app and then developer can upload to firebase distribution (for android) or upload IOS app to test flight
- option 2 : devops can help build android app and upload automatically to firebase distribution using jenkins(example), but this option need a bigger server to run the build mobile app.

# Logging for service
we can capturing, analyzing, and securely storing transaction log data using opensource tools like, prometheus, promtail, grafana and loki. we can send service log to loki and then we can see the log on the grafana dashboard, and we can also make alert for log that you want to know earlier

# CI/CD 
if infra setup is complete, we need CI/CD to automate deployment process.
- `cd deployment`
- we have file name **.gitlab-ci.yml** it's can automatically run if developer push to git.
