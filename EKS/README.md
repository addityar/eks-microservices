# Deploy Services 
In this section, we assume that your service has been build as docker image, and your service can connect to database.<br>

**1. Deploy all backend services**
- `cd EKS/backend`
- `kubectl apply -f backend1.yaml`
- `kubectl apply -f backend2.yaml`
- `kubectl apply -f backend3.yaml`
- `kubectl apply -f backend4.yaml`<br>

**2. Deploy frontend service**
- `cd EKS/frontend`
- `kubectl apply -f frontend1.yaml`
- if frontend service can be access to public you can apply ingress `cd ingress` `kubectl apply -f ingress-frontend1.yaml`<br>

**3. For Mobile service i have 2 option** 
- option 1 : developer can build react app on the local to generate .apk/.app/IOS app and then developer can upload to firebase distribution (for android) or upload IOS app to test flight
- option 2 : devops can help build android app and upload automatically to firebase distribution using jenkins(example), but this option need a bigger server to run the build mobile app.
