provider "aws" {
  region = "ap-southeast-1"  # Change to your desired AWS region
}

module "eks" {
  source          = "terraform-aws-modules/eks/aws"
  cluster_name    = "my-eks-cluster"
  subnets         = ["subnet-xxxx", "subnet-yyyy"]  # Replace with your subnet IDs
  vpc_id          = "vpc-xxxx"  # Replace with your VPC ID
  cluster_version = "1.21"

  node_groups = {
    eks_nodes = {
      desired_capacity = 2
      max_capacity     = 3
      min_capacity     = 1

      instance_type = "t3a.small"  # Change to your desired instance type
    }
  }
}

module "istio" {
  source = "terraform-aws-modules/eks/aws//modules/eks-cluster"

  cluster_id = module.eks.cluster_id
  subnets    = ["subnet-xxxx", "subnet-yyyy"]  # Replace with your subnet IDs
  vpc_id     = "vpc-xxxx"  # Replace with your VPC ID

  istio_namespace = "istio-system"
  istio_profile   = "default"
}

resource "aws_security_group" "eks_ingress_sg" {
  name        = "eks-ingress-sg"
  description = "Security group for EKS ingress"
  vpc_id      = module.eks.vpc_id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = -1
    cidr_blocks = ["192.168.1.1/32"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = -1
    cidr_blocks = ["192.168.1.1/32"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

# Associate Security Group with EKS Cluster
resource "aws_eks_cluster" "eks" {
  name     = module.eks.cluster_id
  role_arn = module.eks.cluster_oidc_issuer_url

  depends_on = [
    module.eks,
  ]

  vpc_config {
    subnet_ids = module.eks.subnet_ids
    security_group_ids = [
      aws_security_group.eks_ingress_sg.id,
    ]
  }
}

resource "kubernetes_namespace" "microservices" {
  metadata {
    name = "microservices"
  }
}