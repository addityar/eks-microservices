provider "aws" {
  region = "ap-southeast-1" # Change to your desired AWS region
}

resource "aws_db_instance" "my_rds_instance" {
  identifier            = "my-postgres-db"
  allocated_storage    = 20
  storage_type          = "gp2"
  engine               = "postgres"
  engine_version       = "13.4"
  instance_class       = "db.t2.micro" # Change to your desired instance type
  username             = "admin"      # Change to your desired username
  password             = "your_password" # Change to your desired password
  publicly_accessible  = false
  multi_az             = false
  skip_final_snapshot  = true

  vpc_security_group_ids = ["sg-xxxx"] # Specify your security group IDs

  tags = {
    Name = "MyPostgresDB"
  }
}

# outputs.tf

output "rds_endpoint" {
  value = aws_db_instance.my_rds_instance.endpoint
}

output "rds_instance_id" {
  value = aws_db_instance.my_rds_instance.id
}